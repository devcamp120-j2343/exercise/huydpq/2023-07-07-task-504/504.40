import Car from './Car.js';

class ElectricCar extends Car {
  constructor(make, model, batteryLevel) {
    super(make, model);
    this.batteryLevel = batteryLevel;
  }

  drive() {
    console.log(`Driving ${this.make} ${this.model} - Battery Level: ${this.batteryLevel}%`);
  }

  charge() {
    console.log(`Charging ${this.make} ${this.model}`);
  }
}

export default ElectricCar;
