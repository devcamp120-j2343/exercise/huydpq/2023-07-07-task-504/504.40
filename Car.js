class Car {
    constructor(make, model) {
      this.make = make;
      this.model = model;
    }
  
    drive() {
      console.log(`Driving ${this.make} ${this.model}`);
    }
  }
  
  export default Car;
  